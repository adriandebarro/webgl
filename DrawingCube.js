var VSHADER =
'attribute vec4 a_Position; \n' +
'attribute vec4 a_Color; \n' +
'uniform mat4 u_MVPMatrix; \n' +
'varying vec4 v_Color;\n' +
'void main(){\n' +
	// it is important to check the order of the operations, since u might get a completly different end result!!
	'	gl_Position =  u_MVPMatrix * a_Position;\n ' +
    '	v_Color = a_Color;\n' +
'}\n';

var FSHADER = 'precision mediump float;' +
 	'varying vec4 v_Color;' +
	'void main(){ \n' +
	'gl_FragColor = v_Color;\n' +
'}\n';


var initExampleVertexBuffer = function(gl){

    var verticesColors = new Float32Array([
        // Vertex coordinates and color
        1.0,  1.0,  1.0,     1.0,  1.0,  1.0,  // v0 White
        -1.0,  1.0,  1.0,     1.0,  0.0,  1.0,  // v1 Magenta
        -1.0, -1.0,  1.0,     1.0,  0.0,  0.0,  // v2 Red
        1.0, -1.0,  1.0,     1.0,  1.0,  0.0,  // v3 Yellow
        1.0, -1.0, -1.0,     0.0,  1.0,  0.0,  // v4 Green
        1.0,  1.0, -1.0,     0.0,  1.0,  1.0,  // v5 Cyan
        -1.0,  1.0, -1.0,     0.0,  0.0,  1.0,  // v6 Blue
        -1.0, -1.0, -1.0,     0.0,  0.0,  0.0   // v7 Black
    ]);

    var indices = new Uint8Array([
    	0,1,2, 0,2,3,
		0,3,4, 0,4,5,
		0,5,6, 0,6,1,
		1,6,7, 1,7,2,
		7,4,3, 7,3,2,
		4,7,6, 4,6,5
	]);

	return initVertexBuffers(gl, verticesColors, indices);
}


var initVertexBuffers = function(gl, vertexDetails, indices) {
	var vertexColorBuffer = gl.createBuffer();
	var indexBuffer = gl.createBuffer();

	if(!vertexColorBuffer)
	{
		logError("Error when creating vertex Buffer")
	}

	gl.bindBuffer(gl.ARRAY_BUFFER, vertexColorBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, vertexDetails, gl.STATIC_DRAW);

	var elementSize = vertexDetails.BYTES_PER_ELEMENT;

	var a_Position = gl.getAttribLocation(gl.program, 'a_Position');
	var a_Color = gl.getAttribLocation(gl.program, 'a_Color');

	if(a_Position < 0 || a_Color < 0)
	{
		logError("Error occured when retreiving attributes");
	}

	gl.vertexAttribPointer(a_Position, 3, gl.FLOAT, false, elementSize * 6, 0);
	gl.enableVertexAttribArray(a_Position);

    gl.vertexAttribPointer(a_Color, 3, gl.FLOAT, false, elementSize * 6, elementSize * 3);
    gl.enableVertexAttribArray(a_Color);

	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, indices, gl.STATIC_DRAW);

	return indices.length;
};


var main = function(){

	debugger;
	var canvas = document.getElementById("webgl");

	var gl = getWebGLContext(canvas);

	if(!initShaders(gl, VSHADER, FSHADER))
	{
		alert("Error occured with WebGl context");
		return;
	}

	var n = initExampleVertexBuffer(gl);

	if(!n || n == 0)
	{
		logError("Failed to initroduce vertex details");
		return;
	}

	var u_MVPMatrix = gl.getUniformLocation(gl.program, 'u_MVPMatrix');

	if(!u_MVPMatrix)// || !u_ModelMatrix)
	{
		logError("Failed to retreive one of the uniforms");
		return;
	}

	// solves the HiddenSurface Artefact
	gl.enable(gl.DEPTH_TEST);

	var modelRotation = new Matrix4().setRotate(-29, 0,1,0).rotate(30, 1,0,0);

	var viewMatrix = new Matrix4();
		viewMatrix.setLookAt(0,0,10, 0,0,-100, 0,1,0);

	var projectionMatrix = new Matrix4();
	projectionMatrix.setPerspective(30, canvas.width / canvas.height, 1, 100);

	var mvpMatrix = new Matrix4().multiply(projectionMatrix).multiply(viewMatrix).multiply(modelRotation);

	gl.uniformMatrix4fv(u_MVPMatrix, false, mvpMatrix.elements);


    gl.clearColor(0,0,0,1);

    // clearing the Depth Buffer
    gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);
	gl.drawElements(gl.TRIANGLES, n, gl.UNSIGNED_BYTE, 0);
}


var logError = function(message) {
	alert(message);
	console.log(message);
	return;
}