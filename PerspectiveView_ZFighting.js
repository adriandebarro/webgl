// TODO fix this add the ZFIghting solution and understand the issue how it is solved

var VSHADER =
'attribute vec4 a_Position; \n' +
'attribute vec4 a_Color; \n' +
'uniform mat4 u_ModelMatrix; \n' +
'uniform mat4 u_ViewMatrix;\n' +
'uniform mat4 u_ProjMatrix;\n' +
'varying vec4 v_Color;\n' +
'void main(){\n' +
	// it is important to check the order of the operations, since u might get a completly different end result!!
	'	gl_Position =  u_ProjMatrix * u_ViewMatrix * a_Position;\n ' +
    '	v_Color = a_Color;\n' +
'}\n';

var FSHADER = 'precision mediump float;' +
 	'varying vec4 v_Color;' +
	'void main(){ \n' +
	'gl_FragColor = v_Color;\n' +
'}\n';


var initExampleVertexBuffer = function(gl){

	var vertexDetails = new Float32Array([
        // Three triangles on the right side

        0.0,  1.0,   -5.0,  0.4,  0.4,  1.0,  // The front blue one
        0.5, -1.0,  -5.0,  0.4,  0.4,  1.0,
        -0.5, -1.0,  -5.0,  1.0,  0.4,  0.4,

        0.0,  0.25,   -5.0,  0,  0.4,  1.0,  // The front blue one
        0.25, -0.25,  -5.0,  1,  0.4,  1.0,
        -0.25, -0.25,  -5.0,  1.0, 1,  0.4,
    ]);

	return initVertexBuffers(gl, vertexDetails);
}


var initVertexBuffers = function(gl, vertexDetails) {
	var size = 6;

	var vertexColorBuffer = gl.createBuffer();
	if(!vertexColorBuffer)
	{
		logError("Error when creating vertex Buffer")
	}

	gl.bindBuffer(gl.ARRAY_BUFFER, vertexColorBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, vertexDetails, gl.STATIC_DRAW);

	var elementSize = vertexDetails.BYTES_PER_ELEMENT;

	var a_Position = gl.getAttribLocation(gl.program, 'a_Position');
	var a_Color = gl.getAttribLocation(gl.program, 'a_Color');

	if(a_Position < 0 || a_Color < 0)
	{
		logError("Error occured when retreiving attributes");
	}

	gl.vertexAttribPointer(a_Position, 3, gl.FLOAT, false, elementSize * 6, 0);
	gl.enableVertexAttribArray(a_Position);

    gl.vertexAttribPointer(a_Color, 3, gl.FLOAT, false, elementSize * 6, elementSize * 3);
    gl.enableVertexAttribArray(a_Color);

	return size;
};


var main = function(){

	debugger;
	var canvas = document.getElementById("webgl");

	var gl = getWebGLContext(canvas);

	if(!initShaders(gl, VSHADER, FSHADER))
	{
		alert("Error occured with WebGl context");
		return;
	}

	var n = initExampleVertexBuffer(gl);

	if(!n || n == 0)
	{
		logError("Failed to initroduce vertex details");
		return;
	}

	var u_ViewMatrix = gl.getUniformLocation(gl.program, 'u_ViewMatrix');
	var u_projectionMatrix = gl.getUniformLocation(gl.program, 'u_ProjMatrix');
	//var u_ModelMatrix = gl.getUniformLocation(gl.program, 'u_ModelMatrix');
	
	if(!u_ViewMatrix || !u_projectionMatrix)// || !u_ModelMatrix)
	{
		logError("Failed to retreive one of the uniforms");
		return;
	}

	// solves the HiddenSurface Artefact
	gl.enable(gl.DEPTH_TEST);

	var modelRotation = new Matrix4().setRotate(-29, 0,0,1);

	var viewMatrix = new Matrix4();
		viewMatrix.setLookAt(0,0,0, 0,0,-100, 0,1,0);

	var projectionMatrix = new Matrix4();
	projectionMatrix.setPerspective(30, canvas.width / canvas.height, 1, 100);

	gl.uniformMatrix4fv(u_ViewMatrix,false, viewMatrix.elements);
    gl.uniformMatrix4fv(u_projectionMatrix,false, projectionMatrix.elements);
    //gl.uniformMatrix4fv(u_ModelMatrix,false, modelRotation.elements);


    gl.clearColor(0,0,0,1);

    // clearing the Depth Buffer
    gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);

    // enable polygon offset fill
    gl.enable(gl.POLYGON_OFFSET_FILL);

	gl.drawArrays(gl.TRIANGLES, 0, n/2);

	// setting the polygon offset
	gl.polygonOffset(1.0, 1.0);

    gl.drawArrays(gl.TRIANGLES, n/2, n/2);

}


var logError = function(message) {
	alert(message);
	console.log(message);
	return;
}