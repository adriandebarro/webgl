// TODO solve the issue in this code,
// Normal seems to be 0, since setting the cube to be the color of the normal, it got al black

var VSHADER =
'attribute vec4 a_Position; \n' +
'attribute vec4 a_Color; \n' +
'attribute vec4 a_Normal; \n' +
'uniform mat4 u_MVPMatrix; \n' +
'uniform vec3 u_LightColor; \n' +
'uniform vec3 u_LightDirection; \n' +
'uniform vec4 u_CubeColor; \n' +
'uniform mat4 u_NormalMatrix;\n' +
'uniform vec3 u_AmbientLight;\n' +

'varying vec4 v_Color;\n' +
'void main(){\n' +
    // diffuse shading = cos the angle between the normal and the incoming light directiokn * the light color * msurface color (albedo)
	// it is important to check the order of the operations, since u might get a completly different end result!!
    'gl_Position =  u_MVPMatrix * a_Position;\n ' +
    'vec3 normal = normalize(vec3(u_NormalMatrix * a_Normal));\n' +
    // capping the value of the dot product between 1 and 0
    'float lDotN =  max(dot(u_LightDirection, normal), 0.0);\n' +
    // diffuse light i s the product of the dot product between the surface normal and the light direction * the light color * albedo
    'vec3 diffuseComponent = lDotN * u_LightColor * vec3(1.0, 0.0,0.0); \n' +
    // whilst ambient light is the product of the albedo and ambient light
    'vec3 ambientComponent = u_AmbientLight * vec3(1.0, 0.0,0.0); \n' +
    'v_Color = vec4(diffuseComponent + ambientComponent, a_Color.a);\n' +
    //'v_Color = vec4(normal, a_Color.a);\n' +
'}\n';

var FSHADER = 'precision mediump float;' +
 	'varying vec4 v_Color;' +
	'void main(){ \n' +
	'gl_FragColor = v_Color;\n' +
'}\n';


var initExampleVertexBuffer = function(gl){
    var vertices = new Float32Array([   // Vertex coordinates
        1.0, 1.0, 1.0,  -1.0, 1.0, 1.0,  -1.0,-1.0, 1.0,   1.0,-1.0, 1.0,  // v0-v1-v2-v3 front
        1.0, 1.0, 1.0,   1.0,-1.0, 1.0,   1.0,-1.0,-1.0,   1.0, 1.0,-1.0,  // v0-v3-v4-v5 right
        1.0, 1.0, 1.0,   1.0, 1.0,-1.0,  -1.0, 1.0,-1.0,  -1.0, 1.0, 1.0,  // v0-v5-v6-v1 up
        -1.0, 1.0, 1.0,  -1.0, 1.0,-1.0,  -1.0,-1.0,-1.0,  -1.0,-1.0, 1.0,  // v1-v6-v7-v2 left
        -1.0,-1.0,-1.0,   1.0,-1.0,-1.0,   1.0,-1.0, 1.0,  -1.0,-1.0, 1.0,  // v7-v4-v3-v2 down
        1.0,-1.0,-1.0,  -1.0,-1.0,-1.0,  -1.0, 1.0,-1.0,   1.0, 1.0,-1.0   // v4-v7-v6-v5 back
    ]);

    var colors = new Float32Array([     // Colors
        0.4, 0.4, 1.0,  0.4, 0.4, 1.0,  0.4, 0.4, 1.0,  0.4, 0.4, 1.0,  // v0-v1-v2-v3 front(blue)
        0.4, 1.0, 0.4,  0.4, 1.0, 0.4,  0.4, 1.0, 0.4,  0.4, 1.0, 0.4,  // v0-v3-v4-v5 right(green)
        1.0, 0.4, 0.4,  1.0, 0.4, 0.4,  1.0, 0.4, 0.4,  1.0, 0.4, 0.4,  // v0-v5-v6-v1 up(red)
        1.0, 1.0, 0.4,  1.0, 1.0, 0.4,  1.0, 1.0, 0.4,  1.0, 1.0, 0.4,  // v1-v6-v7-v2 left
        1.0, 1.0, 1.0,  1.0, 1.0, 1.0,  1.0, 1.0, 1.0,  1.0, 1.0, 1.0,  // v7-v4-v3-v2 down
        0.4, 1.0, 1.0,  0.4, 1.0, 1.0,  0.4, 1.0, 1.0,  0.4, 1.0, 1.0   // v4-v7-v6-v5 back
    ]);

    var normals = new Float32Array([    // Normal
        0.0, 0.0, 1.0,   0.0, 0.0, 1.0,   0.0, 0.0, 1.0,   0.0, 0.0, 1.0,  // v0-v1-v2-v3 front
        1.0, 0.0, 0.0,   1.0, 0.0, 0.0,   1.0, 0.0, 0.0,   1.0, 0.0, 0.0,  // v0-v3-v4-v5 right
        0.0, 1.0, 0.0,   0.0, 1.0, 0.0,   0.0, 1.0, 0.0,   0.0, 1.0, 0.0,  // v0-v5-v6-v1 up
        -1.0, 0.0, 0.0,  -1.0, 0.0, 0.0,  -1.0, 0.0, 0.0,  -1.0, 0.0, 0.0,  // v1-v6-v7-v2 left
        0.0,-1.0, 0.0,   0.0,-1.0, 0.0,   0.0,-1.0, 0.0,   0.0,-1.0, 0.0,  // v7-v4-v3-v2 down
        0.0, 0.0,-1.0,   0.0, 0.0,-1.0,   0.0, 0.0,-1.0,   0.0, 0.0,-1.0   // v4-v7-v6-v5 back
    ]);

    var indices = new Uint8Array([       // Indices of the vertices
        0, 1, 2,   0, 2, 3,    // front
        4, 5, 6,   4, 6, 7,    // right
        8, 9,10,   8,10,11,    // up
        12,13,14,  12,14,15,    // left
        16,17,18,  16,18,19,    // down
        20,21,22,  20,22,23     // back
    ]);

    if(!initArrayBuffer(gl, normals, 3, gl.FLOAT, 'a_Normal'))
    {
        logError("Error occured whilst creating a_Position");
        return false;
    }

    if(!initArrayBuffer(gl, vertices, 3, gl.FLOAT, 'a_Position'))
	{
		logError("Error occured whilst creating a_Position");
		return false;
	}

    if(!initArrayBuffer(gl, colors, 3, gl.FLOAT, 'a_Color'))
    {
        logError("Error occured whilst creating a_Color");
        return false;
    }

    var indexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, indices, gl.STATIC_DRAW);

    return indices.length;
}

var initArrayBuffer = function(gl, arrayElements, singleElmentSize, type,  attributeName)
{
	var elementSize = arrayElements.BYTES_PER_ELEMENT;
	var dataBuffer = gl.createBuffer();

	if(!dataBuffer)
	{
		logError("Failed ot create attribute buffer");
		return false;
	}

	gl.bindBuffer(gl.ARRAY_BUFFER, dataBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, arrayElements, gl.STATIC_DRAW);

    var attribute = gl.getAttribLocation(gl.program, attributeName);

    if(attribute < 0 || attribute == null)
    {
        logError("Error occured when retreiving attribute");
    	return false;
    }

    gl.vertexAttribPointer(attribute, singleElmentSize, type, false, 0, 0);
    gl.enableVertexAttribArray(attribute);

    return true;
}

var main = function(){

	var canvas = document.getElementById("webgl");

	var gl = getWebGLContext(canvas);

	if(!initShaders(gl, VSHADER, FSHADER))
	{
		alert("Error occured with WebGl context");
		return;
	}

	var n = initExampleVertexBuffer(gl);

	if(!n || n == 0)
	{
		logError("Failed to initroduce vertex details");
		return;
	}

	var u_MVPMatrix = gl.getUniformLocation(gl.program, 'u_MVPMatrix');

	if(!u_MVPMatrix)// || !u_ModelMatrix)
	{
		logError("Failed to retreive one of the uniforms");
		return;
	}

	var lightDirection = new Vector3([0.0, 3.0, 4.0]);
    lightDirection.normalize();


	var u_LightColor = gl.getUniformLocation(gl.program, 'u_LightColor');
    var u_LightDirection = gl.getUniformLocation(gl.program, 'u_LightDirection');
    var u_AmbientLight = gl.getUniformLocation(gl.program, 'u_AmbientLight');
    var u_NormalMatrix = gl.getUniformLocation(gl.program, 'u_NormalMatrix');

	if(!u_LightColor || !u_LightDirection || !u_AmbientLight || !u_NormalMatrix)
    {
        logError('One of the new attributes failed');
        return;
    }

    gl.uniform3f(u_LightColor, 1.0, 1.0, 1.0);
    gl.uniform3fv(u_LightDirection, lightDirection.elements);
    gl.uniform3f(u_AmbientLight, 0.2, 0.2, 0.2);

	// solves the HiddenSurface Artefact
	gl.enable(gl.DEPTH_TEST);

	var modelRotation = new Matrix4().setTranslate(0,0.9,0).rotate(90, 0,0,1);


	var viewMatrix = new Matrix4();
		viewMatrix.setLookAt(-7, 2.5, 6, 0, 0, 0, 0, 1, 0);

	var projectionMatrix = new Matrix4();
	projectionMatrix.setPerspective(30, canvas.width / canvas.height, 1, 300);

	var mvpMatrix = new Matrix4().multiply(projectionMatrix).multiply(viewMatrix).multiply(modelRotation);

    var normalMatrix = new Matrix4().setInverseOf(modelRotation);
    normalMatrix.transpose();

    gl.uniformMatrix4fv(u_NormalMatrix, false, normalMatrix.elements);
    gl.uniformMatrix4fv(u_MVPMatrix, false, mvpMatrix.elements);

    gl.clearColor(0,0,0,1);

    // clearing the Depth Buffer
    gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);
	gl.drawElements(gl.TRIANGLES, n, gl.UNSIGNED_BYTE, 0);
}


var logError = function(message) {
	alert(message);
	console.log(message);
	return;
}