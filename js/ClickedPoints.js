

// for the length of the  canvas will be sending 
// only a single position
var VSHADER_SOURCE = 'attribute vec4 a_Position; \n' +
'void main(){ \n'+
'	gl_Position = a_Position;\n'+
'	gl_PointSize = 10.0; \n' +
'}';

// for each position received add the following
// color
var FSHADER_PROGRAM = 'void main() {\n' + 
	'gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0); \n'+
'}\n';


var main = function()
{
	var canvas = document.getElementById("webgl");

	if(!canvas)
	{
		return;
	}

	var gl = getWebGLContext(canvas);

	if(!gl)
	{
		console.log("Failed to get the rendering context for webgl");
		return;
	}

	if(!initShaders(gl, VSHADER_SOURCE, FSHADER_PROGRAM)){
		console.log('Failed to inistalize shaders');
		return;
	}

	// getting the handler of a_Position
	var a_position = gl.getAttribLocation(gl.program, 'a_Position');

	// if handler id is smaller than 0 than it means it was not found
	if(a_position < 0)
	{
		debugger;
		console.log('Failed to get the storage location of a_Position');
		return;
	}

	canvas.onmousedown = function(ev) {click(ev, gl, canvas, a_position)};


	gl.clearColor(0.0, 0.0, 0.0, 1.0);

	gl.clear(gl.COLOR_BUFFER_BIT);
};


var g_points = [];

function click(ev, gl, canvas, a_position)
{

	debugger;
	var x = ev.clientX;
	var y = ev.clientY;

	var rect = ev.target.getBoundingClientRect();

	x = ((x - rect.left) - canvas.height/2)/(canvas.height/2);
	y = (canvas.width/2 - (y - rect.top))/(canvas.width / 2);

	g_points.push(x);
	g_points.push(y);

	// specifying to webgl to use the COLOR_BUFFER when clearing the canvas
	gl.clear(gl.COLOR_BUFFER_BIT);

	var length = g_points.length;

	for(var pointIndex = 0; pointIndex < length; pointIndex+=2)
	{
		debugger;
		var x = g_points[pointIndex];
		var y = g_points[pointIndex + 1];
		gl.vertexAttrib3f(a_position, x, y, 0.0);
		gl.drawArrays(gl.POINTS, 0, g_points.length);
	}
}	
