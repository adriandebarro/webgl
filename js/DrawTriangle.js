var main = function()
{
	var canvas = document.getElementById("example");

	if(!canvas)
	{
		console.log("Cannot retreive the canvas element");
	}

	// getting the 2D contect
	var ctx = canvas.getContext("2d");

	// setting th filling style
	ctx.fillStyle = 'rgba(0,0,255, 1.0)';
	// filling at the following bounds
	ctx.fillRect(120, 10, 150, 150);
}