var main = function()
{
	var canvas = document.getElementById("webgl");

	if(!canvas)
	{
		return;
	}

	var gl = getWebGLContext(canvas);

	if(!gl)
	{
		console.log("Failed to get the rendering context for webgl");
		return;
	}

	console.log(gl);

	gl.clearColor(0.0, 0.0, 1.0, 1.0);

	// specifying to webgl to use the COLOR_BUFFER when clearing the canvas
	gl.clear(gl.COLOR_BUFFER_BIT);
};