

// for the length of the  canvas will be sending 
// only a single position
var VSHADER_SOURCE = 'void main(){ \n'+
'	gl_Position = vec4(0.0, 0.0, 0.0, 1.0);\n'+
'	gl_PointSize = 10.0; \n' +
'}';

// for each position received add the following
// color
var FSHADER_PROGRAM = 'void main() {\n' + 
	'gl_FragColor = vec4(1.0, 1.0, 0.0, 1.0); \n'+
'}\n';


var main = function()
{
	var canvas = document.getElementById("webgl");

	if(!canvas)
	{
		return;
	}

	var gl = getWebGLContext(canvas);

	if(!gl)
	{
		console.log("Failed to get the rendering context for webgl");
		return;
	}

	debugger;
	if(!initShaders(gl, VSHADER_SOURCE, FSHADER_PROGRAM)){
		console.log('Failed to inistalize shaders');
		return;
	}

	gl.clearColor(0.0, 0.0, 0.0, 1.0);

	// specifying to webgl to use the COLOR_BUFFER when clearing the canvas
	gl.clear(gl.COLOR_BUFFER_BIT);

	gl.drawArrays(gl.POINTS, 0, 1);
};