

// for the length of the  canvas will be sending 
// only a single position
var VSHADER_SOURCE = 'attribute vec4 a_Position; \n' +
'attribute float a_PointSize; \n ' +  
'void main(){ \n'+
'	gl_Position = a_Position;\n'+
'	gl_PointSize = a_PointSize; \n' +
'}';

// for each position received add the following
// color
var FSHADER_PROGRAM = 'void main() {\n' + 
	'gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0); \n'+
'}\n';


var main = function()
{
	var canvas = document.getElementById("webgl");

	if(!canvas)
	{
		return;
	}

	var gl = getWebGLContext(canvas);

	if(!gl)
	{
		console.log("Failed to get the rendering context for webgl");
		return;
	}

	if(!initShaders(gl, VSHADER_SOURCE, FSHADER_PROGRAM)){
		console.log('Failed to inistalize shaders');
		return;
	}

	// getting the handler of a_Position
	var a_position = gl.getAttribLocation(gl.program, 'a_Position');
	var a_pointSize = gl.getAttribLocation(gl.program, 'a_PointSize');

	// if handler id is smaller than 0 than it means it was not found
	if(a_position < 0)
	{
		debugger;
		console.log('Failed to get the storage location of a_Position');
		return;
	}

	if(a_pointSize < 0)
	{
		debugger;
		console.log('Failed to get the storage location of a_PointSize');
		return;
	}

	gl.vertexAttrib3f(a_position, 0.0, 0.5, 0.0);
	gl.vertexAttrib1f(a_pointSize, 20.0);

	gl.clearColor(0.0, 0.0, 0.0, 1.0);

	// specifying to webgl to use the COLOR_BUFFER when clearing the canvas
	gl.clear(gl.COLOR_BUFFER_BIT);

	gl.drawArrays(gl.POINTS, 0, 1);
};