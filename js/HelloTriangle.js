

// for the length of the  canvas will be sending 
// only a single position
var VSHADER_SOURCE = 'attribute vec4 a_Position; \n' +
'void main(){ \n'+
'	gl_Position = a_Position;\n'+
'}';

// for each position received add the following
// color
var FSHADER_PROGRAM = 'void main() {\n' + 
	'gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0); \n'+
'}\n';


var main = function()
{
	debugger;
	var canvas = document.getElementById("webgl");

	if(!canvas)
	{
		return;
	}

	var gl = getWebGLContext(canvas);

	if(!gl)
	{
		console.log("Failed to get the rendering context for webgl");
		return;
	}

	if(!initShaders(gl, VSHADER_SOURCE, FSHADER_PROGRAM)){
		console.log('Failed to inistalize shaders');
		return;
	}

	gl.clearColor(0.0, 0.0, 0.0, 1.0);

	gl.clear(gl.COLOR_BUFFER_BIT);

	var drawNumber = initMultiPointVertexBuffer(gl);

	gl.drawArrays(gl.TRIANGLES, 0, drawNumber);
};

var initVertexBuffers = function(gl, vertices)
{
	debugger;
	var n = 3;
	var vertexBuffer = gl.createBuffer();
	
	if(!vertexBuffer)
	{	
		console.log("Failed to create a vertex Buffer");
		return -1;
	}

	gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

	var a_position = gl.getAttribLocation(gl.program, 'a_Position');

	if(a_position < 0)
	{
		console.log("Failed to retreive the a_position attirbute");
		return -1;
	}
	gl.vertexAttribPointer(a_position, 2, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(a_position);

	return n;
};

var initMultiPointVertexBuffer = function(gl){

    // in case you want to draw a triangle 
	//var g_points = new Float32Array([-0.5, 0.5, 0.5, 0.5, -0.5, -0.5, -0.5, -0.5, 0.5, -0.5, 0.5, 0.5 ]);	
	var g_points = new Float32Array([0, 0.2, -0.2, -0.2, 0.2, -0.2]);	
	var numberVertices = 3;

	return initVertexBuffers(gl, g_points);
};

main();
