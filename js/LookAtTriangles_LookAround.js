// var VSHADER_SOURCE =
// 'precision highp float;' +
// 'attribute vec4 a_Position;\n' +
// 'attribute vec4 a_Color;\n' +
// 'uniform mat4 u_ModelMatrix; \n' +
// 'uniform mat4 u_ViewMatrix; \n' +
// 'varying vec4 v_Color;\n' +
// 'void main(){ \n'+
// 	'gl_Position = u_ViewMatrix * u_ModelMatrix * a_Position;\n'+
// 	'v_Color = a_Color;\n' +
// '}';

var VSHADER_SOURCE =
    'precision highp float;' +
    'attribute vec4 a_Position;\n' +
    'attribute vec4 a_Color;\n' +
    'uniform mat4 u_ProjectionMatrix;\n' +
    'uniform mat4 u_ModelViewMatrix;\n' +
    'varying vec4 v_Color;\n' +
'void main(){ \n'+
    'gl_Position = u_ModelViewMatrix * u_ProjectionMatrix * a_Position;\n'+
    //'gl_Position = u_ProjectionMatrix * a_Position;\n'+
    'v_Color = a_Color;\n' +
'}';


// for each position received add the following
// color
// Accessing the texture can be done through the use of the uniform sampler2D variable
// by using the texture2D function
var FSHADER_PROGRAM =
'precision highp float;\n' +
'varying vec4 v_Color; \n' +
'void main() {\n' +
    // //'gl_FragColor = 0.7 * texture2D(u_Sampler, v_TexCoord) + (0.3 * v_Color); \n'+
    // 'vec4 color1 = texture2D(u_Sampler, v_TexCoord);\n' +
    // 'vec4 color2 = texture2D(u_TemplateSampler, v_TexCoord);\n' +
	'gl_FragColor = v_Color; \n'+
	//'gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);' +
'}\n';


var g_eyeX = 0.2, g_eyeY = 0.25, g_eyeZ = 0.25, g_near = 0, g_far = 0.5;

var main = function()
{
	var canvas = document.getElementById("webgl");

	if(!canvas)
	{
		return;
	}

	var gl = getWebGLContext(canvas);

	if(!gl)
	{
		console.log("Failed to get the rendering context for webgl");
		return;
	}

	if(!initShaders(gl, VSHADER_SOURCE, FSHADER_PROGRAM)){
		console.log('Failed to inistalize shaders');
		return;
	}

	var drawNumber = initMultiPointVertexBuffer(gl);

	var modelViewMatrix = new Matrix4()
        .setLookAt(g_eyeX, g_eyeY, g_eyeZ, 0, 0, 0, 0, 1, 0);

	// going to add the orthographic view

    var projectionMatrix = new Matrix4()
        .setOrtho(-1,1,-1,1, g_near, g_far);


    var u_modelViewMatrix = gl.getUniformLocation(gl.program, 'u_ModelViewMatrix');

    var u_projectionMatrix = gl.getUniformLocation(gl.program, 'u_ProjectionMatrix');

    if(u_modelViewMatrix < 0)
    {
        logError("Error occured while getting the MVP uniform variable");
        return;
    }

    if(u_projectionMatrix < 0)
    {
        logError("Error occured while getting the MVP uniform variable");
        return;
    }

    document.onkeydown = function(ev){
        keyDown(ev, gl, drawNumber, u_modelViewMatrix, modelViewMatrix, u_projectionMatrix, projectionMatrix);
    };

    draw(gl, drawNumber, u_modelViewMatrix, modelViewMatrix, u_projectionMatrix, projectionMatrix);
};


var draw = function (gl, n, u_ViewMatrix, viewMatrix, u_projectionMatrix, projectionMatrix) {
    debugger;
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    gl.clear(gl.COLOR_BUFFER_BIT);

    viewMatrix.setLookAt(g_eyeX, g_eyeY, g_eyeZ, 0, 0, 0, 0, 1, 0);

    projectionMatrix.setOrtho(-1,1,-1,1, g_near, g_far);

    gl.uniformMatrix4fv(u_ViewMatrix, false, viewMatrix.elements);

    gl.uniformMatrix4fv(u_projectionMatrix, false, projectionMatrix.elements);

    gl.drawArrays(gl.TRIANGLES, 0, n);
};

var keyDown = function(ev, gl, n, u_viewMatrix, viewMatrix, u_projectionMatrix, projectionMatrix) {

    debugger;
    if(ev.keyCode == 39)
    {
        g_eyeX -= 0.01;
    }
    else if(ev.keyCode == 40)
    {
        g_eyeZ -= 0.01;
    }
    else if(ev.keyCode == 38)
    {
        g_eyeZ += 0.01;
    }
    else if(ev.keyCode == 37)
    {
        g_eyeX += 0.01;
    }
    // w and s control the far component
    else if(ev.keyCode == 87 )
    {
        g_far += 0.01;
    }
    else if(ev.keyCode == 83)
    {
        g_far -= 0.01;
    }
    else if(ev.keyCode == 65)
    {
        g_near += 0.01;
    }
    else if(ev.keyCode == 68)
    {
        g_near -= 0.01;
    }
    else
    {
        return;
    }

    var statsElement = document.getElementById("stats");
    statsElement.innerHTML = "g_eyeX: "+roundNumber(g_eyeX)+" g_eyeY: "+roundNumber(g_eyeY)+" g_eyeZ: "+roundNumber(g_eyeZ)+" g_near: "+roundNumber(g_near)+" g_far: "+roundNumber(g_far);


    draw(gl, n, u_viewMatrix, viewMatrix, u_projectionMatrix, projectionMatrix);
};

var roundNumber = function(number) {
    return Number((number).toFixed(3));
}

var initUniforms = function (gl) {

    var viewMatrix = new Matrix4().setLookAt(g_eyeX, g_eyeY, g_eyeZ, 0,0,0, 0,1,0);

    var modelMatrix = new Matrix4().rotate(-40, 0,0,1);

    var modelViewMatrix = viewMatrix.multiply(modelMatrix);

    if(!setUniformMatrix4f(gl, "u_ModelViewMatrix", modelViewMatrix.elements))
	{
		return false;
	}

    return true;
}

/*
* The way w eimplemented the initVertexBuffers is highly inefficient
* we should minimize the amount of bufferes we create by adding everything in one single buffer
* */
var initVertexBuffers = function(gl,vertices)
{;
	var n = 9;
	var vertexBuffer = gl.createBuffer();

	if(!vertexBuffer)
	{	
		console.log("Failed to create a vertex Buffer");
		return -1;
	}

	gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

	var a_position = gl.getAttribLocation(gl.program, 'a_Position');

	if(a_position < 0)
	{
		console.log("Failed to retreive the a_position attirbute");
		return -1;
	}

	var ELEMENTSIZE = vertices.BYTES_PER_ELEMENT;

	gl.vertexAttribPointer(a_position, 3, gl.FLOAT, false, ELEMENTSIZE * 6, 0);
	gl.enableVertexAttribArray(a_position);

    gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

    var a_color = gl.getAttribLocation(gl.program, 'a_Color');

    if(a_color < 0)
    {
        logError("Failed to retreive the a_color attribute");
        return -1;
    }

    gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(a_color, 3, gl.FLOAT, false, ELEMENTSIZE * 6, ELEMENTSIZE * 3);
    gl.enableVertexAttribArray(a_color);

    return n;
};

var initMultiPointVertexBuffer = function(gl){

	var details = new Float32Array([
		0.0, 0.5, -0.4, 0.4, 1.0, 0.4,
        -0.5, -0.5, -0.4, 0.4, 1.0, 0.4,
        0.5, -0.5, -0.4, 1.0, 0.4, 0.4,

        0.5, 0.4, -0.2, 1.0, 0.4, 0.4,
        -0.5, 0.4, -0.2, 1.0, 1.0, 0.4,
        0.0, -0.6,  -0.2,  1.0,  1.0,  0.4,

        0.0,  0.5, 0.0, 0.4 , 0.4,  1.0,
        -0.5, -0.5, 0.0, 0.4, 0.4, 1.0,
        0.5, -0.5, -0.0, 1.0, 0.4, 0.4
	]);

	return initVertexBuffers(gl, details);
};

var g_image1 = false;
var g_image2 = false;

var initTexture = function (gl, n) {
	var texture = gl.createTexture();
	var texture1 = gl.createTexture();

	if(texture < 0)
	{
		logError("Texture variable could not be created");
		return false;
	}

	if(texture1 < 0)
	{
		logError("Texture1 variable could not be created succesfully");
		return false;
	}

	var u_sampler = gl.getUniformLocation(gl.program, 'u_Sampler');
    var u_sampler1 = gl.getUniformLocation(gl.program, 'u_TemplateSampler');


    if(u_sampler < 0)
	{
        logError("Failed to retreive the u_sampler uniform");
        return false;
	}


    if(u_sampler1 < 0)
    {
        logError("Failed to retreive the u_TemplateSampler uniform");
        return false;
    }

	var image = new Image();
    var image1 = new Image();

    if(!image)
	{
        logError ("Error whilst creating image");
		return false;
	}

    if(!image1)
    {
        logError ("Error whilst creating image2");
        return false;
    }

    image.src = 'C://Users/Adrian/Desktop/WebGL/resources/sky.jpg';
    image1.src = 'C://Users/Adrian/Desktop/WebGL/resources/circle.gif';

    image.addEventListener('load', loadTexture(gl, n, texture, u_sampler, image, 0), false);
    image1.addEventListener('load', loadTexture(gl, n, texture1, u_sampler1, image1,1), false);

    return true;
};

var logError = function(errorMessage) {
	alert(errorMessage);
	console.log(errorMessage);
}
var g_texUnit0 = false, g_texUnit1 = false;
var loadTexture = function(gl, n, texture, uniformVariable, image, texUnit){

	// the coordinate system used in images is usually different to that used in
	// Webgl therefore we have to flip the axis such that it properly set
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);

    /**
	 * 	WebGl supports multiple textures this is dealt with through the use
	 * 	of so called texture units number of texture units supported by WebGL
	 * 	is dependent on the hardware, however by default you have access to at
	 * 	least 8 texture unit
     */
    debugger;

    if(texUnit == 0)
	{
		// activte texture unit 0
		gl.activeTexture(gl.TEXTURE0);
		g_texUnit0 = !g_texUnit0;
	}
	else if(texUnit == 1)
	{
		// activate tecture unit 1
        gl.activeTexture(gl.TEXTURE1);
        g_texUnit1 = !g_texUnit1;
    }
	else
	{
		// throw an error
		logError("Error occured whilst tying to active texture unit "+texUnit)
		return;
	}

    /*
    * This is specifyinh the type of texture that we are dealing with in
    * <note>You should take a look at the Opengl ES programming guide for more info </note>
    * Basically what is happening we stating to the Webgl state machine that we activated the textureunit 0
    * And not what were doing is specifying the type of texture and what texture that we
    * are injecting into Texture unit 0
    * */
    gl.bindTexture(gl.TEXTURE_2D, texture);

    /*
    * There are different methods of that can be done
    * MAgnify, reduce, wrap left and right, and wrap top and botton
    * while the third parameter specifies how should the alteration be executed,
    * In this case the minification should be made in a linear fashion
    * */
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);

    /*
    * Assigning a texture image to the texture object
    *  Basically we are just setting the RGB vales of what the texture should expect
    */
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);

    // telling the WebGL pipeline to attach the texture unit 0 to our sampler
    gl.uniform1i(uniformVariable, texUnit);

    if(g_texUnit0 && g_texUnit1)
	{
        // since we were dependant on the image to be loaded, we have to issue the draw command here
        gl.drawArrays(gl.TRIANGLE_STRIP, 0, n);
	}

};

var setUniformMatrix4f = function(gl, variable, value){

    var uniformLocation = gl.getUniformLocation(gl.program, variable);

    if(uniformLocation < 0)
    {
        logError("failed to get the uniform "+variable);
        return false;
    }

    gl.uniformMatrix4fv(uniformLocation, false, value);

    return true;
};

var setUniformVariable = function(gl, variable, value){

    debugger;
    var uniformLocation = gl.getUniformLocation(gl.program, variable);

    if(uniformLocation < 0)
    {
        logError("failed to get the uniform "+variable);
        return false;
    }

    gl.uniform1f(uniformLocation, value);

    return true;
};
