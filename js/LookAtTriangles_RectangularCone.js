
var VSHADER_SOURCE =
    'precision highp float;' +
    'attribute vec4 a_Position;\n' +
    'attribute vec4 a_Color;\n' +
    'uniform mat4 u_ProjectionMatrix;\n' +
    'uniform mat4 u_ModelViewMatrix;\n' +
    'varying vec4 v_Color;\n' +
'void main(){ \n'+
    'gl_Position = u_ProjectionMatrix * u_ModelViewMatrix * a_Position;\n'+
    //'gl_Position = u_ModelViewMatrix * a_Position;\n'+
    //'gl_Position = u_ProjectionMatrix * a_Position;\n'+
    'v_Color = a_Color;\n' +
'}';

// for each position received add the following
// color
// Accessing the texture can be done through the use of the uniform sampler2D variable
// by using the texture2D function
var FSHADER_PROGRAM =
'precision highp float;\n' +
'varying vec4 v_Color; \n' +
'void main() {\n' +
    // //'gl_FragColor = 0.7 * texture2D(u_Sampler, v_TexCoord) + (0.3 * v_Color); \n'+
    // 'vec4 color1 = texture2D(u_Sampler, v_TexCoord);\n' +
    // 'vec4 color2 = texture2D(u_TemplateSampler, v_TexCoord);\n' +
	'gl_FragColor = v_Color; \n'+
	//'gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);' +
'}\n';

var main = function()
{
	var canvas = document.getElementById("webgl");

	var gl = getWebGLContext(canvas);
	if(!gl){
		console.log("Failed to get the rendering context for webgl");
		return;
	}

	if(!initShaders(gl, VSHADER_SOURCE, FSHADER_PROGRAM)){
		logError('Failed to inistalize shaders');
		return;
	}

	var drawNumber = initMultiPointVertexBuffer(gl);
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    var u_modelViewMatrix = gl.getUniformLocation(gl.program, 'u_ModelViewMatrix');
    var u_projectionMatrix = gl.getUniformLocation(gl.program, 'u_ProjectionMatrix');
    if(u_projectionMatrix < 0 || u_modelViewMatrix < 0)
    {
        logError("Error occured while getting the "+u_projectionMatrix < 0? "ProjMatrix " : "ModelView " +"uniform variable");
        return;
    }

    var modelViewMatrix = new Matrix4();
    var projectionMatrix = new Matrix4();

    modelViewMatrix.setLookAt(0, 0, 5, 0, 0, -100, 0, 1, 0);
    debugger;
    projectionMatrix.setPerspective(30, canvas.width/canvas.height, 1, 100);

    gl.uniformMatrix4fv(u_modelViewMatrix, false, modelViewMatrix.elements);
    gl.uniformMatrix4fv(u_projectionMatrix, false, projectionMatrix.elements);

    gl.clear(gl.COLOR_BUFFER_BIT);
    gl.drawArrays(gl.TRIANGLES, 0, drawNumber);
};


/*
* The way w eimplemented the initVertexBuffers is highly inefficient
* we should minimize the amount of bufferes we create by adding everything in one single buffer
* */
var initVertexBuffers = function(gl,vertices)
{
	var n = 18;
	var vertexBuffer = gl.createBuffer();
	if(!vertexBuffer)
	{	
		console.log("Failed to create a vertex Buffer");
		return -1;
	}

	gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

	var a_position = gl.getAttribLocation(gl.program, 'a_Position');

	if(a_position < 0)
	{
		console.log("Failed to retreive the a_position attirbute");
		return -1;
	}

	var ELEMENTSIZE = vertices.BYTES_PER_ELEMENT;

	gl.vertexAttribPointer(a_position, 3, gl.FLOAT, false, ELEMENTSIZE * 6, 0);
	gl.enableVertexAttribArray(a_position);

    var a_color = gl.getAttribLocation(gl.program, 'a_Color');

    if(a_color < 0)
    {
        logError("Failed to retreive the a_color attribute");
        return -1;
    }

    gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(a_color, 3, gl.FLOAT, false, ELEMENTSIZE * 6, ELEMENTSIZE * 3);
    gl.enableVertexAttribArray(a_color);

    return n;
};

var initMultiPointVertexBuffer = function(gl){

    var numberTriangles = 24;

    var newArray = new Float32Array(numberTriangles * 3);

    var layer = 1;
    for(var index = 0; index < numberTriangles; index += 6)
    {
        newArray.push([ 0.75,  1.0,  (layer*-4.0),  0.4,  1.0,  0.4, // The back green one
            0.25, -1.0,  (layer*-4.0),  0.4,  1.0,  0.4,
            1.25, -1.0,  (layer*-4.0),  1.0,  0.4,  0.4,

            0.75,  1.0,  (layer*-2.0),  1.0,  1.0,  0.4, // The middle yellow one
            0.25, -1.0,  (layer*-2.0),  1.0,  1.0,  0.4,
            1.25, -1.0,  (layer*-2.0),  1.0,  0.4,  0.4,

            0.75,  1.0,   (layer*-1.0),  0.4,  0.4,  1.0,  // The front blue one
            0.25, -1.0,   (layer*-1.0),  0.4,  0.4,  1.0,
            1.25, -1.0,   (layer*-1.0),  1.0,  0.4,  0.4,

            // Three triangles on the left side
            -0.75,  1.0,  (layer*-4.0),  0.4,  1.0,  0.4, // The back green one
            -1.25, -1.0,  (layer*-4.0),  0.4,  1.0,  0.4,
            -0.25, -1.0,  (layer*-4.0),  1.0,  0.4,  0.4,

            -0.75,  1.0,  (layer*-2.0),  1.0,  1.0,  0.4, // The middle yellow one
            -1.25, -1.0,  (layer*-2.0),  1.0,  1.0,  0.4,
            -0.25, -1.0,  (layer*-2.0),  1.0,  0.4,  0.4,

            -0.75,  1.0,   (layer*-1.0),  0.4,  0.4,  1.0,  // The front blue one
            -1.25, -1.0,   (layer*-1.0),  0.4,  0.4,  1.0,
            -0.25, -1.0,   (layer*-1.0),  1.0,  0.4,  0.4]);
        layer ++;
    }

	var details = new Float32Array([
        0.75,  1.0,  -4.0,  0.4,  1.0,  0.4, // The back green one
        0.25, -1.0,  -4.0,  0.4,  1.0,  0.4,
        1.25, -1.0,  -4.0,  1.0,  0.4,  0.4,

        0.75,  1.0,  -2.0,  1.0,  1.0,  0.4, // The middle yellow one
        0.25, -1.0,  -2.0,  1.0,  1.0,  0.4,
        1.25, -1.0,  -2.0,  1.0,  0.4,  0.4,

        0.75,  1.0,   0.0,  0.4,  0.4,  1.0,  // The front blue one
        0.25, -1.0,   0.0,  0.4,  0.4,  1.0,
        1.25, -1.0,   0.0,  1.0,  0.4,  0.4,

        // Three triangles on the left side
        -0.75,  1.0,  -4.0,  0.4,  1.0,  0.4, // The back green one
        -1.25, -1.0,  -4.0,  0.4,  1.0,  0.4,
        -0.25, -1.0,  -4.0,  1.0,  0.4,  0.4,

        -0.75,  1.0,  -2.0,  1.0,  1.0,  0.4, // The middle yellow one
        -1.25, -1.0,  -2.0,  1.0,  1.0,  0.4,
        -0.25, -1.0,  -2.0,  1.0,  0.4,  0.4,

        -0.75,  1.0,   0.0,  0.4,  0.4,  1.0,  // The front blue one
        -1.25, -1.0,   0.0,  0.4,  0.4,  1.0,
        -0.25, -1.0,   0.0,  1.0,  0.4,  0.4,
    ]);

	return initVertexBuffers(gl, details);
};

var logError = function(errorMessage) {
	alert(errorMessage);
	console.log(errorMessage);
};