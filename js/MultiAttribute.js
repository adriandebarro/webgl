

// for the length of the  canvas will be sending 
// only a single position
var VSHADER_SOURCE = 'attribute vec4 a_Position; \n' +
'attribute float a_PointSize;\n'+
'void main(){ \n'+
'	gl_Position = a_Position;\n'+
'	gl_PointSize = a_PointSize; \n' +
'}';

// for each position received add the following
// color
var FSHADER_PROGRAM = 'void main() {\n' + 
	'gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0); \n'+
'}\n';


var main = function()
{
	debugger;
	var canvas = document.getElementById("webgl");

	if(!canvas)
	{
		return;
	}

	var gl = getWebGLContext(canvas);

	if(!gl)
	{
		console.log("Failed to get the rendering context for webgl");
		return;
	}

	if(!initShaders(gl, VSHADER_SOURCE, FSHADER_PROGRAM)){
		console.log('Failed to inistalize shaders');
		return;
	}

	gl.clearColor(0.0, 0.0, 0.0, 1.0);

	gl.clear(gl.COLOR_BUFFER_BIT);

	var drawNumber = initMultiPointVertexBuffer(gl);
	debugger;
	gl.drawArrays(gl.POINTS, 0, 3);
};

/*
* The way w eimplemented the initVertexBuffers is highly inefficient
* we should minimize the amount of bufferes we create by adding everything in one single buffer
* */
var initVertexBuffers = function(gl,vertices)
{
	debugger;
	var n = 3;
	var vertexBuffer = gl.createBuffer();

	if(!vertexBuffer)
	{	
		console.log("Failed to create a vertex Buffer");
		return -1;
	}

	var pointSizesBuffer = gl.createBuffer();

	if(!pointSizesBuffer)
	{
		alert("Failed to create pointSizeBuffer");
	}

	gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

	var a_position = gl.getAttribLocation(gl.program, 'a_Position');

	if(a_position < 0)
	{
		console.log("Failed to retreive the a_position attirbute");
		return -1;
	}

	var ELEMENTSIZE = vertices.BYTES_PER_ELEMENT;

	gl.vertexAttribPointer(a_position, 2, gl.FLOAT, false, ELEMENTSIZE * 3, 0);
	gl.enableVertexAttribArray(a_position);

    debugger;
    gl.bindBuffer(gl.ARRAY_BUFFER, pointSizesBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

    var a_pointSizes = gl.getAttribLocation(gl.program, 'a_PointSize');

    if(a_pointSizes < 0)
    {
    	alert("Failed to retreive the a_pointSizes attribute");
        console.log("Failed to retreive the a_pointSizes attirbute");
        return -1;
    }

    /*
    * sincje were passing thej elements to WebGl via the same buffer
    * we need to specify where eache element starts  and where it finishes, to do this we need to do the following
    * Specify the Stride and the offset
    * Stride, specifies the size of each set fo elements, while offset specifies the jump required to tget to the elements in each pack
    * // on this case we are specifiying a stride of 3 elements and an offset of two elements
    * The Term ELEMENT SIZE is just the byte amount of each float element that we know is 4 bytes per float
    * */

    gl.vertexAttribPointer(a_pointSizes, 1, gl.FLOAT, false, ELEMENTSIZE * 3, ELEMENTSIZE * 2);
    gl.enableVertexAttribArray(a_pointSizes);

    return n;
};

var initMultiPointVertexBuffer = function(gl){

	var details = new Float32Array([0, 0.5,2.0, -0.5, -0.5, 10.0, 0.5, -0.5, 20.0]);
	var numberVertices = 3;

	return initVertexBuffers(gl, details);
};
