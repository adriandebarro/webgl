var VSHADER_SOURCE =
'precision highp float;' +
'attribute vec4 a_Position; \n' +
'attribute vec4 a_Color;\n' +
'varying vec4 v_Color;\n' +
'void main(){ \n'+
	'gl_Position = a_Position;\n'+
	'v_Color = a_Color; \n' +
	'gl_PointSize = 10.0; \n' +
'}';

// for each position received add the following
// color
var FSHADER_PROGRAM =
'precision highp float;' +
	'varying vec4 v_Color; \n' +
'void main() {\n' +
	'gl_FragColor = v_Color; \n'+
//'gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);' +
	'}\n';


var main = function()
{
	debugger;
	var canvas = document.getElementById("webgl");

	if(!canvas)
	{
		return;
	}

	var gl = getWebGLContext(canvas);

	if(!gl)
	{
		console.log("Failed to get the rendering context for webgl");
		return;
	}

	if(!initShaders(gl, VSHADER_SOURCE, FSHADER_PROGRAM)){
		console.log('Failed to inistalize shaders');
		return;
	}

	gl.clearColor(0.0, 0.0, 0.0, 1.0);

	gl.clear(gl.COLOR_BUFFER_BIT);

	var drawNumber = initMultiPointVertexBuffer(gl);
	debugger;
	gl.drawArrays(gl.TRIANGLES, 0, 3);
};

/*
* The way w eimplemented the initVertexBuffers is highly inefficient
* we should minimize the amount of bufferes we create by adding everything in one single buffer
* */
var initVertexBuffers = function(gl,vertices)
{
	debugger;
	var n = 3;
	var vertexBuffer = gl.createBuffer();

	if(!vertexBuffer)
	{	
		console.log("Failed to create a vertex Buffer");
		return -1;
	}

	gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

	var a_position = gl.getAttribLocation(gl.program, 'a_Position');

	if(a_position < 0)
	{
		console.log("Failed to retreive the a_position attirbute");
		return -1;
	}

	var ELEMENTSIZE = vertices.BYTES_PER_ELEMENT;

	gl.vertexAttribPointer(a_position, 2, gl.FLOAT, false, ELEMENTSIZE * 5, 0);
	gl.enableVertexAttribArray(a_position);

    gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

    debugger;
    var a_color = gl.getAttribLocation(gl.program, 'a_Color');

    if(a_color < 0)
    {
        alert("Failed to retreive the a_color attribute");
        console.log("Failed to retreive the a_color attirbute");
        return -1;
    }

    gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(a_color, 3, gl.FLOAT, false, ELEMENTSIZE * 5, ELEMENTSIZE * 2);
    gl.enableVertexAttribArray(a_color);

    return n;
};

var initMultiPointVertexBuffer = function(gl){

	var details = new Float32Array([0.0, 0.5, 1.0, 0.0, 0.0,
		 							-0.5, -0.5, 0.0, 1.0, 0.0,
									0.5, -0.5, 0.0, 0.0, 1.0]);
	var numberVertices = 3;

	return initVertexBuffers(gl, details);
};
