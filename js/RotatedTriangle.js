// ERROR: 0:8: 'assign' : cannot convert from 'const int' to 'highp float'

var VSHADER_SOURCE = 'attribute vec4 a_Position; \n' +
'uniform vec4 u_Translation; \n'+
'uniform float u_cosB, u_sinB; \n' +
'void main(){ \n'+
'	gl_Position.x = (a_Position.x * u_cosB - a_Position.y * u_sinB);\n'+
'	gl_Position.y = (a_Position.x * u_sinB + a_Position.y * u_cosB);\n'+
'	gl_Position.z = a_Position.z;\n'+
'	gl_Position.w = 1.0;\n'+
'}\n';

// for each position received add the following
// color
var FSHADER_PROGRAM = 'void main() {\n' + 
	'gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0); \n'+
'}\n';


var main = function()
{
	debugger;
	var canvas = document.getElementById("webgl");

	if(!canvas)
	{
		return;
	}

	var gl = getWebGLContext(canvas);

	if(!gl)
	{
		console.log("Failed to get the rendering context for webgl");
		return;
	}

	if(!initShaders(gl, VSHADER_SOURCE, FSHADER_PROGRAM)){
		console.log('Failed to inistalize shaders');
		return;
	}

	gl.clearColor(0.0, 0.0, 0.0, 1.0);

	gl.clear(gl.COLOR_BUFFER_BIT);

    // set rotations variables 
    debugger;
    var radiance = (Math.PI * -90) / 180;
    var cosB = Math.cos(radiance);
    var sinB = Math.sin(radiance);

    if(setUniformVariable(gl, "u_cosB", cosB))
    {
        console.log("Failed to set uniform cosB");
        return -1;
    }

    if(setUniformVariable(gl, "u_sinB", sinB))
    {
        console.log("Failed to set uniform sinB");
        return -1;
    }

	var drawNumber = initMultiPointVertexBuffer(gl);

	gl.drawArrays(gl.TRIANGLES, 0, drawNumber);
};

var initVertexBuffers = function(gl, vertices)
{
	debugger;
	var n = 3;
	var vertexBuffer = gl.createBuffer();
	
	if(!vertexBuffer)
	{	
		console.log("Failed to create a vertex Buffer");
		return -1;
	}

	gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

	var a_position = gl.getAttribLocation(gl.program, 'a_Position');

	if(a_position < 0)
	{
		console.log("Failed to retreive the a_position attirbute");
		return -1;
	}
	gl.vertexAttribPointer(a_position, 2, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(a_position);

	return n;
};

var initMultiPointVertexBuffer = function(gl){

	var g_points = new Float32Array([0, 0.5, -0.5, -0.5, 0.5, -0.5]);	
	var numberVertices = 3;

	return initVertexBuffers(gl, g_points);
};

var setUniformVariable = function(gl, variable, value){

    debugger;
    var uniformLocation = gl.getUniformLocation(gl.program, variable);
    
    if(uniformLocation < 0)
    {
        console.log("failed to get the uniform");
        return true;
    }

    gl.uniform1f(uniformLocation, value);

    return false;
};


main();
