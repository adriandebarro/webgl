// ERROR: 0:8: 'assign' : cannot convert from 'const int' to 'highp float'

var VSHADER_SOURCE = 'attribute vec4 a_Position; \n' +
    'uniform mat4 u_xTranfromMatrix; \n' +
    'void main(){ \n'+
    ' gl_Position = u_xTranfromMatrix * a_Position; \n'+
    '}\n';

// for each position received add the following
// color
var FSHADER_PROGRAM = 'void main() {\n' +
    'gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0); \n'+
    '}\n';


var main = function()
{
    var canvas = document.getElementById("webgl");

    if(!canvas)
    {
        return;
    }

    var gl = getWebGLContext(canvas);

    if(!gl)
    {
        console.log("Failed to get the rendering context for webgl");
        return;
    }

    if(!initShaders(gl, VSHADER_SOURCE, FSHADER_PROGRAM)){
        console.log('Failed to inistalize shaders');
        return;
    }

    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    gl.clear(gl.COLOR_BUFFER_BIT);

    var n = initMultiPointVertexBuffer(gl);

    var currentAngle = 0;
    var xFormMatrix = new Matrix4();

    var xFormMatrixHandle = getUniformLocation(gl, "u_xTranfromMatrix");

    var tick = function(){
        currentAngle = animate(currentAngle);
        draw(gl, n, currentAngle, xFormMatrix, xFormMatrixHandle);
        requestAnimationFrame(tick);
    };

    tick();
};

var initVertexBuffers = function(gl, vertices)
{
    var n = 3;
    var vertexBuffer = gl.createBuffer();

    if(!vertexBuffer)
    {
        console.log("Failed to create a vertex Buffer");
        return -1;
    }

    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

    var a_position = gl.getAttribLocation(gl.program, 'a_Position');

    if(a_position < 0)
    {
        console.log("Failed to retreive the a_position attirbute");
        return -1;
    }
    gl.vertexAttribPointer(a_position, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(a_position);

    return n;
};

var initMultiPointVertexBuffer = function(gl){

    var g_points = new Float32Array([0, 0.5, -0.5, -0.5, 0.5, -0.5]);
    var numberVertices = 3;

    return initVertexBuffers(gl, g_points);
};

var setUniformVariable = function(gl, variable, value){

    var uniformLocation = gl.getUniformLocation(gl.program, variable);

    if(uniformLocation < 0)
    {
        console.log("failed to get the uniform");
        return true;
    }

    gl.uniform1f(uniformLocation, value);

    return false;
};

var setUniformMatrix4f = function(gl, variable, value){

    var uniformLocation = gl.getUniformLocation(gl.program, variable);

    if(uniformLocation < 0)
    {
        console.log("failed to get the uniform");
        return true;
    }

    gl.uniformMatrix4fv(uniformLocation, false, value);

    return false;
};


var getUniformLocation = function(gl, uniform)
{
    var uniformLocation = gl.getUniformLocation(gl.program, uniform);

    if(uniformLocation < 0)
    {
        alert("Failed to retreive the uniform "+uniform);
        console.log("failed to get the uniform "+uniform);
        return uniformLocation;
    }

    return uniformLocation;
};

// we are drawing the movement for the current angle, we are applying both rotation and translation
var draw = function(gl, n, currentAngle, modelMatrix, transformationHandle) {

    if(transformationHandle < 0)
    {
        alert("Uniform location not found ERROR");
        console.log("Uniform location not found ERROR");
        return -1;
    }

    // adding sin shift to triangle
    // translate degrees to radians
    var radians = (currentAngle * Math.PI) / 180;
    modelMatrix.setTranslate(0,Math.sin(radians/2),0);
    modelMatrix.rotate(currentAngle,0,0,1);
    gl.uniformMatrix4fv(transformationHandle, false, modelMatrix.elements);
    gl.clear(gl.COLOR_BUFFER_BIT);
    gl.drawArrays(gl.TRIANGLES, 0, n);
};


var g_last = Date.now();
// the animate function is taking care of checking how much change has occured sinc ehte last redraw
var animate = function(currentAngle)
{
    // checking the last time difference
    var updatedElapsedTime = Date.now() - g_last;
    //refreshing the g_last
    g_last = Date.now();
    // checking the new angle as a form of a ration
    var updatedAngel =  currentAngle + (updatedElapsedTime * 180) / 1000;
    // reducing the number in the region from 0 to 360 (now its 720 since we increased the persiod of the sign wave)
    return updatedAngel %= 720;
};
