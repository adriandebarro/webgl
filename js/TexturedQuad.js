var VSHADER_SOURCE =
'precision highp float;' +
'attribute vec4 a_Position; \n' +
'attribute vec4 a_Color;\n' +
'attribute vec2 a_TexCoord; \n' +
'varying vec4 v_Color;\n' +
'varying vec2 v_TexCoord;\n'+
'void main(){ \n'+
	'gl_Position = a_Position;\n'+
	'v_Color = a_Color;\n' +
	'v_TexCoord = a_TexCoord;\n'+
'}';

// for each position received add the following
// color
// Accessing the texture can be done through the use of the uniform sampler2D variable
// by using the texture2D function
var FSHADER_PROGRAM =
'precision highp float;\n' +
'uniform sampler2D u_Sampler;\n' +
'varying vec2 v_TexCoord;\n' +
'varying vec4 v_Color; \n' +
'void main() {\n' +
	'gl_FragColor = 0.7 * texture2D(u_Sampler, v_TexCoord) + (0.3 * v_Color); \n'+
//'gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);' +
	'}\n';


var main = function()
{
	var canvas = document.getElementById("webgl");

	if(!canvas)
	{
		return;
	}

	var gl = getWebGLContext(canvas);

	if(!gl)
	{
		console.log("Failed to get the rendering context for webgl");
		return;
	}

	if(!initShaders(gl, VSHADER_SOURCE, FSHADER_PROGRAM)){
		console.log('Failed to inistalize shaders');
		return;
	}

	gl.clearColor(0.0, 0.0, 0.0, 1.0);

	gl.clear(gl.COLOR_BUFFER_BIT);

	var drawNumber = initMultiPointVertexBuffer(gl);

	if(!initTexture(gl, drawNumber))
	{
		alert("Error occured while loading textures");
		console.log("Error occured while initialising the textures");
		return;
	}

};

/*
* The way w eimplemented the initVertexBuffers is highly inefficient
* we should minimize the amount of bufferes we create by adding everything in one single buffer
* */
var initVertexBuffers = function(gl,vertices)
{;
	var n = 4;
	var vertexBuffer = gl.createBuffer();

	if(!vertexBuffer)
	{	
		console.log("Failed to create a vertex Buffer");
		return -1;
	}

	gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

	var a_position = gl.getAttribLocation(gl.program, 'a_Position');

	if(a_position < 0)
	{
		console.log("Failed to retreive the a_position attirbute");
		return -1;
	}

	var ELEMENTSIZE = vertices.BYTES_PER_ELEMENT;

	gl.vertexAttribPointer(a_position, 2, gl.FLOAT, false, ELEMENTSIZE * 7, 0);
	gl.enableVertexAttribArray(a_position);

    gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

    var a_color = gl.getAttribLocation(gl.program, 'a_Color');

    if(a_color < 0)
    {
        alert("Failed to retreive the a_color attribute");
        console.log("Failed to retreive the a_color attirbute");
        return -1;
    }

    gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(a_color, 3, gl.FLOAT, false, ELEMENTSIZE * 7, ELEMENTSIZE * 2);
    gl.enableVertexAttribArray(a_color);

    // adding the texture coordinates to be unputed ot the VertexShader
    var a_texCoord = gl.getAttribLocation(gl.program, 'a_TexCoord');

    if(a_texCoord < 0)
	{
        alert("Failed to retreive the a_texCoord attribute");
        console.log("Failed to retreive the a_texCoord attirbute");
        return -1;
	}

    gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(a_texCoord, 2, gl.FLOAT, false, ELEMENTSIZE * 7, ELEMENTSIZE * 5);
    gl.enableVertexAttribArray(a_texCoord);

    return n;
};

var initMultiPointVertexBuffer = function(gl){

	var details = new Float32Array([-0.5, 0.5, 1.0, 0.0, 0.0, -0.3, 1.7,
									-0.5, -0.5, 1.0, 1.0, 1.0, -0.3, -0.2,
		 							0.5, 0.5, 0.0, 1.0, 0.0, 1.7, 1.7,
							        0.5, -0.5, 0.0, 1.0, 0.0, 1.7, -0.2]);

	return initVertexBuffers(gl, details);
};

var initTexture = function (gl, n) {
	var texture = gl.createTexture();

	if(texture < 0)
	{
		logError("Texture variable could not be created");
		return false;
	}

	var u_sampler = gl.getUniformLocation(gl.program, 'u_Sampler');

	if(u_sampler < 0)
	{
        logError("Failed to retreive the u_sampler uniform");
        return false;
	}

	var image = new Image();

	if(!image)
	{
        logError ("Error whilst creating image");
		return false;
	}

    image.src = 'C://Users/Adrian/Desktop/WebGL/resources/sky.jpg';
    image.addEventListener('load', loadTexture(gl, n, texture, u_sampler, image), false);
	return true;
};

var logError = function(errorMessage) {
	alert(errorMessage);
	console.log(errorMessage);
}

var loadTexture = function(gl, n, texture, uniformVariable, image){
	// the coordinate system used in images is usually different to that used in
	// Webgl therefore we have to flip the axis such that it properly set
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);

    /**
	 * 	WebGl supports multiple textures this is dealt with through the use
	 * 	of so called texture units number of texture units supported by WebGL
	 * 	is dependent on the hardware, however by default you have access to at
	 * 	least 8 texture unit
     */
    //
    gl.activeTexture(gl.TEXTURE0);

    /*
    * This is specifyinh the type of texture that we are dealing with in
    * <note>You should take a look at the Opengl ES programming guide for more info </note>
    * Basically what is happening we stating to the Webgl state machine that we activated the textureunit 0
    * And not what were doing is specifying the type of texture and what texture that we
    * are injecting into Texture unit 0
    * */
    gl.bindTexture(gl.TEXTURE_2D, texture);

    /*
    * There are different methods of that can be done
    * MAgnify, reduce, wrap left and right, and wrap top and botton
    * while the third parameter specifies how should the alteration be executed,
    * In this case the minification should be made in a linear fashion
    * */
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);

    /*
    * Assigning a texture image to the texture object
    *  Basically we are just setting the RGB vales of what the texture should expect
    */
    debugger;
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);

    // telling the WebGL pipeline to attach the texture unit 0 to our sampler
    gl.uniform1i(uniformVariable, 0);

    // since we were dependant on the image to be loaded, we have to issue the draw command here
    gl.drawArrays(gl.TRIANGLE_STRIP, 0, n);

};
