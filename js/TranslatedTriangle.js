

// for the length of the  canvas will be sending 
// only a single position
var VSHADER_SOURCE = 'attribute vec4 a_Position; \n' +
'uniform vec4 u_Translation;'+
'void main(){ \n'+
'	gl_Position = a_Position + u_Translation;\n'+
'}';

// for each position received add the following
// color
var FSHADER_PROGRAM = 'void main() {\n' + 
	'gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0); \n'+
'}\n';


var main = function()
{
	debugger;
	var canvas = document.getElementById("webgl");

	if(!canvas)
	{
		return;
	}

	var gl = getWebGLContext(canvas);

	if(!gl)
	{
		console.log("Failed to get the rendering context for webgl");
		return;
	}

	if(!initShaders(gl, VSHADER_SOURCE, FSHADER_PROGRAM)){
		console.log('Failed to inistalize shaders');
		return;
	}

	gl.clearColor(0.0, 0.0, 0.0, 1.0);

	gl.clear(gl.COLOR_BUFFER_BIT);

    var translation = gl.getUniformLocation(gl.program, "u_Translation");
    gl.uniform4f(translation, 0.5, 0.6,0,1);
    if(translation < 0)
    {
        console.log("failed to get the uniform");
        return -1;
    }

	var drawNumber = initMultiPointVertexBuffer(gl);

	gl.drawArrays(gl.TRIANGLES, 0, drawNumber);
};

var initVertexBuffers = function(gl, vertices)
{
	debugger;
	var n = 3;
	var vertexBuffer = gl.createBuffer();
	
	if(!vertexBuffer)
	{	
		console.log("Failed to create a vertex Buffer");
		return -1;
	}

	gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

	var a_position = gl.getAttribLocation(gl.program, 'a_Position');

	if(a_position < 0)
	{
		console.log("Failed to retreive the a_position attirbute");
		return -1;
	}
	gl.vertexAttribPointer(a_position, 2, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(a_position);

	return n;
};

var initMultiPointVertexBuffer = function(gl){

	var g_points = new Float32Array([0, 0.5, -0.5, -0.5, 0.5, -0.5]);	
	var numberVertices = 3;

	return initVertexBuffers(gl, g_points);
};

main();
